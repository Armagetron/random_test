# random_test #

This is a small test program to experiment with different [boost](http://www.boost.org/) random distributions.

### How do I get set up? ###

The software comes with a CMake build file. You can run:
```
mkdir build && cd build
cmake ..
make
./random_test --log_level=message
```

If you want to visualise the results launch the IPython notebook ```distribution-visualisation.ipynb```.

### How can I contribute ? ###

If you want to add a test case please submit a pull request.

### Developer ###

* Alexander Kammeyer [mail](mailto://a.kammeyer@fu-berlin.de) / [telegram](https://telegram.me/kammeyer)
