import subprocess
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Agg')

result = subprocess.run(['./random_test', '--log_level=message'], stdout=subprocess.PIPE)
result = result.stdout.decode("utf-8")
result = result.split('\n')

strings = []
for s in result:
    if len(s) > 0 and s[0].isdigit():
        strings.append(s)

distrib = []
for s in strings:
    res = s.split(' ')
    nums = []
    for r in res:
        if(r.isnumeric()):
            nums.append(int(r))
    distrib.append(sorted(nums))

count = 0
for d in distrib:
    plt.title("Distribution")
    plt.xlabel('x')
    plt.ylabel('distrib')
    plt.plot( np.arange(0, len(d)) , d, "-", label="d")
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
    plt.grid(True)
    plt.savefig('distrib'+str(count), bbox_inches='tight')
    plt.clf()
    count = count + 1
