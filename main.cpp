#define BOOST_TEST_MODULE random_test
#include <boost/test/included/unit_test.hpp>
#include <boost/random.hpp>
#include <boost/random/random_device.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <vector>
#include <algorithm>
#include <iostream>
#include <string>

struct F {
    F() : gen(0), length(1000) { BOOST_TEST_MESSAGE( "setup fixture" ); }
    ~F() { BOOST_TEST_MESSAGE( "teardown fixture" ); }

    size_t length;
    /*boost::random::random_device*/ boost::random::mt19937 gen;

};

template<class T>
void print_vec(std::vector<T> &v) {
    std::stringstream ss;
    for(T i : v) {
        ss << i << " ";
    }
    ss << std::endl;
    BOOST_TEST_MESSAGE(ss.str());
}

BOOST_FIXTURE_TEST_SUITE(random_test, F)

    BOOST_AUTO_TEST_CASE(permutation)
    {
        BOOST_TEST_MESSAGE("running permutation");

        boost::random::uniform_int_distribution<> dist(0, static_cast<unsigned int>(pow(2, 31) - 1));
        std::vector<unsigned int> v(length);
        std::generate(v.begin(), v.end(), [this, &dist] () -> int { return dist.operator()(gen); });
        boost::range::random_shuffle(v);

        for(unsigned int i : v) {
            BOOST_TEST(i >= dist.min());
            BOOST_TEST(i <= dist.max());
        }

        print_vec(v);
    }

    BOOST_AUTO_TEST_CASE(permutation2)
    {
        BOOST_TEST_MESSAGE("running permutation2");

        std::vector<unsigned int> v(length);
        std::generate(v.begin(), v.end(), [n = 0] () mutable { return n++; });
        boost::range::random_shuffle(v);

        for(unsigned int i : v) {
            BOOST_TEST(i >= 0);
            BOOST_TEST(i < v.size());
        }

        print_vec<>(v);
    }

    BOOST_AUTO_TEST_CASE(permutation3)
    {
        BOOST_TEST_MESSAGE("running permutation3");

        std::vector<unsigned int> v(length);
        std::generate(v.begin(), v.end(), [n = v.size() - 1] () mutable { return n--; });
        boost::range::random_shuffle(v);

        for(unsigned int i : v) {
            BOOST_TEST(i >= 0);
            BOOST_TEST(i < v.size());
        }

        print_vec<>(v);
    }

    BOOST_AUTO_TEST_CASE(uniform_32bit)
    {
        BOOST_TEST_MESSAGE("running uniform_32bit");

        boost::random::uniform_int_distribution<> dist(0, static_cast<unsigned int>(pow(2, 31) - 1));
        std::vector<unsigned int> v(length);
        std::generate(v.begin(), v.end(), [this, &dist] () -> int { return dist.operator()(gen); });

        for(unsigned int i : v) {
            BOOST_TEST(i >= dist.min());
            BOOST_TEST(i <= dist.max());
        }

        print_vec<>(v);
    }

    BOOST_AUTO_TEST_CASE(norm)
    {
        BOOST_TEST_MESSAGE("running norm");

        boost::random::normal_distribution<> dist(0.0, 1.0);
        std::vector<unsigned int> v(length);
        std::generate(v.begin(), v.end(), [this, &dist] () -> int {
            return static_cast<int>(1000 * dist.operator()(gen) + (pow(2, 31) - 1.0)/2.0); });

        for(unsigned int i : v) {
            BOOST_TEST(i >= dist.min());
            BOOST_TEST(i <= dist.max());
        }

        print_vec<>(v);
    }

    BOOST_AUTO_TEST_CASE(expo)
    {
        BOOST_TEST_MESSAGE("running expo");

        boost::random::exponential_distribution<> dist(0.5);
        std::vector<unsigned int> v(length);
        std::generate(v.begin(), v.end(), [this, &dist] () -> int { return static_cast<int>(dist.operator()(gen)); });

        for(unsigned int i : v) {
            BOOST_TEST(i >= dist.min());
            BOOST_TEST(i <= dist.max());
        }

        print_vec<>(v);
    }

BOOST_AUTO_TEST_SUITE_END() //random_test